#!/bin/bash

for i in `ls *.svg`;do 
    j="${i%.*}";
    inkscape --export-pdf=public/$j.pdf --export-png=public/$j.png $j.svg;
done
